﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YoutubeSearch;

namespace BoomBox
{
    /// <summary>
    /// Interaction logic for YTsearch.xaml
    /// </summary>
    public partial class YTsearch : Window
    {
        public YTsearch()
        {
            InitializeComponent();
        }

        private int pageNumber = 1;

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSearch.Text))
            {
                MessageBox.Show("Enter Material Name Please.", "Error");
            }
            else
            {
                VideoSearch items = new VideoSearch();
                List<Video> list = new List<Video>();
                foreach (var item in items.SearchQuery(txtSearch.Text, pageNumber))
                {
                    Video video = new Video();
                    video.Title = item.Title;
                    video.Author = item.Author;
                    video.Url = item.Url;
                    byte[] imageBytes = new WebClient().DownloadData(item.Thumbnail);
                    using (MemoryStream ms = new MemoryStream(imageBytes))
                    {
                        video.Thumbnail = Image.FromStream(ms);

                    }
                    list.Add(video);
                }
                videoBindingSource.DataSource = list;
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            if (pageNumber > 1)
            {
                pageNumber--;
                buttonBack_Click(null, null);
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            pageNumber++;
            buttonSearch.PerformClick();
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var tempUrl = dataGridView.CurrentRow.Cells[3].Value.ToString();
            Form2 myForm = new Form2(tempUrl);
            myForm.StartPosition = this.StartPosition;
            myForm.Size = this.Size;
            this.Hide();
            myForm.ShowDialog();
            this.Show();
        }
    }
}
